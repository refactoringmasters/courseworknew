package exception;

import java.util.logging.Level;
import java.util.logging.Logger;

public class NoSuchRoleException extends Exception {

    private static final Logger logger = Logger.getLogger(NoSuchRoleException.class.getName());

    public NoSuchRoleException(String message, Exception e) {
        super(message, e);
        logger.log(Level.WARNING, e.getMessage(), e);
    }

    public NoSuchRoleException(String message) {
        this(message, null);
    }

    public NoSuchRoleException(Exception e) {
        this(e.getMessage(), e);
    }
}