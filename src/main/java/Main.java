
import configs.AuthenficationConfig;
import dao.*;

import entity.*;

public class Main {
    public static void main(String[] args) throws Exception {
        AccsDao accsDao = new AccsDao();
        LogDao logDao = new LogDao();
        int accid;
        String authorizationHeader = "Token userudfe3ugtafetqe4udm03gcdo9q";
        String token = authorizationHeader
                .substring(AuthenficationConfig.AUTHENTICATION_SCHEME.length()).trim();
        String tokenvalidation = logDao.checkTokenFindEmail(token);
        if (!tokenvalidation.equals("NOACC")) {
            accid = Integer.parseInt(tokenvalidation.substring("user".length()).trim());
            System.out.println(accid);
        } else accid = -1;
        Accs accs = accsDao.findAccById(accid);
        System.out.println(accs.getEmail());
    }
}