package dao;

import entity.*;

import javax.ejb.Stateless;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

@Stateless
public class LogDao extends DAO {

    public UsrLog findUsrLogById(int id) {
        session.beginTransaction();
        UsrLog get = session.get(UsrLog.class, id);
        session.getTransaction().commit();
        return get;
    }

    public AdminLog findAdminLogById(int id) {
        session.beginTransaction();
        AdminLog get = session.get(AdminLog.class, id);
        session.getTransaction().commit();
        return get;
    }

    public KitLog findKitLogById(int id) {
        session.beginTransaction();
        KitLog get = session.get(KitLog.class, id);
        session.getTransaction().commit();
        return get;
    }

    public RestsLog findRestLogById(int id) {
        session.beginTransaction();
        RestsLog get = session.get(RestsLog.class, id);
        session.getTransaction().commit();
        return get;
    }

    //Сохранить новый usrlog
    public void saveNewUsrLog(UsrLog usrlog) {
        session.beginTransaction();
        session.save(usrlog);
        session.getTransaction().commit();
    }

    // Обновить информацию о usrlog
    public void updateUsrLog(UsrLog usrlog) {
        session.beginTransaction();
        session.update(usrlog);
        session.getTransaction().commit();
    }

    public void saveNewKitLog(KitLog kitLog) {
        session.beginTransaction();
        session.save(kitLog);
        session.getTransaction().commit();

    }

    public void saveNewRestLog(RestsLog restsLog) {
        session.beginTransaction();
        session.save(restsLog);
        session.getTransaction().commit();

    }

    public void saveNewAdminLog(AdminLog adminLog) {
        session.beginTransaction();
        session.save(adminLog);
        session.getTransaction().commit();

    }

    //Удалить UsrLog по его ID
    public void deleteUsrLogById(int id) {
        UsrLog usrlog = findUsrLogById(id);
        session.beginTransaction();
        session.delete(usrlog);
        session.getTransaction().commit();
    }

    //Удалить RestLog по ID
    public void deleteRestLogById(int id) {
        RestsLog restsLog = findRestLogById(id);
        session.beginTransaction();
        session.delete(restsLog);
        session.getTransaction().commit();
    }

    //Удалить KitLog по ID
    public void deleteKitLogById(int id) {
        KitLog kitLog = findKitLogById(id);
        session.beginTransaction();
        session.delete(kitLog);
        session.getTransaction().commit();
    }

    //обновить AdminLog по ID
    public void updateAdminLog(AdminLog adminLog) {
        session.beginTransaction();
        session.update(adminLog);
        session.getTransaction().commit();
    }

    //обновить KitLog по ID
    public void updateKitLog(KitLog kitLog) {
        session.beginTransaction();
        session.update(kitLog);
        session.getTransaction().commit();
    }

    //обновить RestLog по ID
    public void updateRestLog(RestsLog restsLog) {
        session.beginTransaction();
        session.update(restsLog);
        session.getTransaction().commit();
    }

    public Accs registrationUsr(String mail, String pass, String name) {
        Accs a = new Accs();
        a.setName(name);
        AccsDao dao = new AccsDao();
        a.setEmail(mail);
        dao.saveNewAcc(a);
        Accs fa = dao.findAccByMail(mail);
        UsrLog u = new UsrLog();
        String sha256hex = org.apache.commons.codec.digest.DigestUtils.sha256Hex(pass);
        u.setHash(sha256hex);
        u.setConfirmation(false);
        u.setId(fa.getId());
        LogDao ld = new LogDao();
        ld.saveNewUsrLog(u);
        return fa;
    }

    public Accs registrationAdmin(String mail, String pass) {
        Accs a = new Accs();
        AccsDao dao = new AccsDao();
        a.setEmail(mail);
        dao.saveNewAcc(a);
        Accs fa = dao.findAccByMail(mail);
        LogDao logDao = new LogDao();
        AdminLog adminLog = new AdminLog();
        String sha256hex = org.apache.commons.codec.digest.DigestUtils.sha256Hex(pass);
        adminLog.setHash(sha256hex);
        adminLog.setId(fa.getId());
        logDao.saveNewAdminLog(adminLog);
        return fa;
    }

    public Accs registrationKit(String mail, String pass, int restid) {
        Accs a = new Accs();
        AccsDao dao = new AccsDao();
        a.setEmail(mail);
        dao.saveNewAcc(a);
        Accs fa = dao.findAccByMail(mail);
        dao.saveAccWithRest(fa.getId(), restid);
        KitLog KitLog = new KitLog();
        String sha256hex = org.apache.commons.codec.digest.DigestUtils.sha256Hex(pass);
        KitLog.setHash(sha256hex);
        KitLog.setId(fa.getId());
        LogDao logDao = new LogDao();
        logDao.saveNewKitLog(KitLog);
        return fa;
    }

    public Accs registrationRest(String mail, String pass, int restid) {
        Accs a = new Accs();
        AccsDao dao = new AccsDao();
        a.setEmail(mail);
        dao.saveNewAcc(a);
        Accs fa = dao.findAccByMail(mail);
        dao.saveAccWithRest(fa.getId(), restid);
        RestsLog RestLog = new RestsLog();
        String sha256hex = org.apache.commons.codec.digest.DigestUtils.sha256Hex(pass);
        RestLog.setHash(sha256hex);
        RestLog.setId(fa.getId());
        LogDao logDao = new LogDao();
        logDao.saveNewRestLog(RestLog);
        return fa;
    }

    public String getUsrHashByAccId(int accid) {
        UsrLog ul = findUsrLogById(accid);
        return ul.getHash();
    }

    public String getAdminHashByAccId(int accid) {
        AdminLog al = findAdminLogById(accid);
        return al.getHash();
    }

    public String getRestHashByAccId(int accid) {
        RestsLog rl = findRestLogById(accid);
        return rl.getHash();
    }

    public String getKitHashByAccId(int accid) {
        KitLog kl = findKitLogById(accid);
        return kl.getHash();
    }

    public boolean validateEmail(String email) {
        List<Accs> ul = (List<Accs>) session.createQuery("From Accs ").list();
        Iterator<Accs> AccsIterator = ul.iterator();
        while (AccsIterator.hasNext()) {
            try {
                Accs accs = AccsIterator.next();
                if (accs.getEmail().equals(email)) {
                    return true;
                }
            } catch (NoSuchElementException e) {
                break;
            }
        }
        return false;
    }

    //Метод возвращает 255555 если нет такого юзера или неправильная роль
    public String checkTokenFindEmail(String token) {
        List<UsrLog> ul = (List<UsrLog>) session.createQuery("From UsrLog ").list();
        List<AdminLog> al = (List<AdminLog>) session.createQuery("From AdminLog ").list();
        List<KitLog> kl = (List<KitLog>) session.createQuery("From KitLog ").list();
        List<RestsLog> rl = (List<RestsLog>) session.createQuery("From RestsLog ").list();
        String role = " ";
        int id = 19999;
        Iterator<UsrLog> usrLogIterator = ul.iterator();
        while (usrLogIterator.hasNext()) {
            try {
                UsrLog usrLog = usrLogIterator.next();
                String tk = usrLog.getToken();
                if (tk != null) {
                    if (tk.equals(token)) {
                        role = "user";
                        id = usrLog.getId();
                        return role + " " + id;
                    }
                }
            } catch (NoSuchElementException e) {
                break;
            }
        }
        Iterator<AdminLog> adminLogIterator = al.iterator();
        while (adminLogIterator.hasNext()) {
            AdminLog adminLog = adminLogIterator.next();
            try {
                String tka = adminLog.getToken();
                if (tka == (null)) break;
                if (tka.equals(token)) {
                    role = "admin";
                    id = adminLog.getId();
                    return role + " " + id;
                }
            } catch (NoSuchElementException e) {
                break;
            }
        }
        Iterator<KitLog> kitLogIterator = kl.iterator();
        while (kitLogIterator.hasNext()) {
            try {
                KitLog kitLog = kitLogIterator.next();
                if (kitLog.getToken().equals(token)) {
                    role = "kitchen";
                    id = kitLog.getId();
                    return role + " " + id;
                }
            } catch (NoSuchElementException e) {
                break;
            }
        }
        Iterator<RestsLog> restsLogIterator = rl.iterator();
        while (restsLogIterator.hasNext()) {
            try {
                RestsLog restsLog = restsLogIterator.next();
                if (restsLog.getToken().equals(token)) {
                    role = "rest";
                    id = restsLog.getId();
                    return role + " " + id;
                }
            } catch (NoSuchElementException e) {
                break;
            }
        }
        return "NOACC";
    }

    public KitLog findKitLogByMail(String mail) {
        AccsDao ad = new AccsDao();
        Accs a = ad.findAccByMail(mail);
        return findKitLogById(a.getId());
    }

    public RestsLog findRestsLogByMail(String mail) {
        AccsDao ad = new AccsDao();
        Accs a = ad.findAccByMail(mail);
        return findRestLogById(a.getId());
    }

    public UsrLog findUsrLogByMail(String mail) {
        AccsDao ad = new AccsDao();
        Accs a = ad.findAccByMail(mail);
        return findUsrLogById(a.getId());
    }

    public AdminLog findAdminLogByMail(String mail) {
        AccsDao ad = new AccsDao();
        Accs a = ad.findAccByMail(mail);
        return findAdminLogById(a.getId());
    }
}