package dao;

import entity.*;
import org.hibernate.Query;

import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Stateless
public class AccsDao extends DAO {

    public Accs findAccById(int id) {
        session.beginTransaction();
        Accs get = session.get(Accs.class, id);
        session.getTransaction().commit();
        return get;
    }

    //Сохранить новый аккаунт
    public void saveNewAcc(Accs accs) {
        session.beginTransaction();
        session.save(accs);
        session.getTransaction().commit();
    }

    // Обновить информацию об аккаунте
    public void updateAcc(Accs accs) {
        session.beginTransaction();
        session.update(accs);
        session.getTransaction().commit();
    }

    //Удалить аккаунт по его ID
    public void deleteAccById(int id) {
        Accs accs = findAccById(id);
        session.beginTransaction();
        session.delete(accs);
        session.getTransaction().commit();
    }

    public void saveAccWithRest(int AccId, int RestId) {
        AccsRests ar = new AccsRests();
        ar.setRestsId(RestId);
        ar.setAccountsId(AccId);
        session.beginTransaction();
        session.saveOrUpdate(ar);
        session.getTransaction().commit();
    }

    //Получить все Аккаунты
    public List<Accs> findAllAccs() {
        List<Accs> accs = (List<Accs>) session.createQuery("From Accs ").list();
        return accs;
    }

    public List<Reviews> findAllAccReviewsById(int id) {
        session.beginTransaction();
        Accs accs = findAccById(id);
        return (List<Reviews>) accs.getReviewsById();
    }

    public void addReviewToAcc(Reviews r, int accid) {
        Accs acc = findAccById(accid);
        session.beginTransaction();
        Collection<Reviews> rv = acc.getReviewsById();
        rv.add(r);
        session.update(acc);
        session.getTransaction().commit();
    }

    public void addVisitToAcc(Visits v, int accid) {
        Accs acc = findAccById(accid);
        session.beginTransaction();
        Collection<Visits> rv;
        try {
            rv = acc.getVisitsById();
        } catch (NullPointerException npe) {
            rv = new ArrayList<Visits>();
        }
        rv.add(v);
        session.update(acc);
        session.getTransaction().commit();
    }

    public List<Reviews> findAllReviewsByAccId(int accid) {
        Accs accs = findAccById(accid);
        return (List<Reviews>) accs.getReviewsById();

    }

    public Accs findAccByMail(String mail) {
        Query tq = session.createQuery("SELECT c FROM Accs c WHERE c.email = :mail");
        List<Accs> a = tq.setParameter("mail", mail).list();
        return a.get(0);

    }
}

