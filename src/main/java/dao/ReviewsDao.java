package dao;

import entity.Accs;
import entity.Rests;
import entity.Reviews;
import org.hibernate.Query;

import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class ReviewsDao extends DAO {

    //Найти отзыв по ID
    public Reviews findReviewById(int id) {
        session.beginTransaction();
        Reviews get = session.get(Reviews.class, id);
        session.getTransaction().commit();
        return get;
    }

    //Сохранить новый отзыв
    public void saveNewReview(Reviews reviews) {
        session.beginTransaction();
        session.save(reviews);
        session.getTransaction().commit();
    }

    // Обновить информацию об отзыве
    public void updateReviews(Reviews reviews) {
        session.beginTransaction();
        session.update(reviews);
        session.getTransaction().commit();
    }

    //Удалить отзыв по его ID
    public void deleteReviewById(int id) {
        session.beginTransaction();
        Reviews reviews = findReviewById(id);
        session.delete(reviews);
        session.getTransaction().commit();
    }

    //Получить все отзывы
    public List<Reviews> findAllAccs() {
        List<Reviews> reviews = (List<Reviews>) session.createQuery("From Reviews ").list();
        return reviews;
    }

    public List<Reviews> getReviewsForResr(int restid) {
        Query tq = session.createQuery("select c From Reviews c where c.restsId = :restid ");
        tq.setParameter("restid", restid);
        return tq.list();
    }

    public void setReviewForRestForAcc(Reviews r, int accid, int restid) {
        AccsDao ac = new AccsDao();
        ac.addReviewToAcc(r, accid);
        Accs acc = ac.findAccById(accid);
        System.out.println(acc.getId());
        ac.destroySession();
        RestsDao rd = new RestsDao();
        rd.addReviewToRest(r, restid);
        Rests rest = rd.findRestById(restid);
        rd.destroySession();
        session.beginTransaction();
        r.setAccsByAccountsId(acc);
        r.setRestsByRestsId(rest);
        session.save(r);
        session.getTransaction().commit();
    }
}
