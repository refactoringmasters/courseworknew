package dao;

import entity.*;
import org.hibernate.Query;

import javax.ejb.Stateless;
import java.util.Collection;
import java.util.List;

@Stateless
public class RestsDao extends DAO {

    //Найти ресторан по его ID
    public Rests findRestById(int id) {
        session.beginTransaction();
        Rests get = session.get(Rests.class, id);
        session.getTransaction().commit();
        return get;
    }

    //Сохранить новый ресторан
    public void saveNewRest(Rests r) {
        session.beginTransaction();
        session.save(r);
        session.getTransaction().commit();
    }

    // Обновить информацию о ресторане
    public void updateRest(Rests r) {
        session.beginTransaction();
        session.update(r);
        session.getTransaction().commit();
    }

    //Добавить блюдо к ресторану
    public void addMenuItemToRest(Rests r, Menu m) {
        session.beginTransaction();
        Collection<Menu> menuCollection = r.getMenusById();
        System.out.println(menuCollection == null);
        System.out.println(r.getName());
        menuCollection.add(m);
        r.setMenusById(menuCollection);
        session.update(r);
        m.setRestsByRestsId(r);
        session.save(m);
        session.getTransaction().commit();
    }

    //Получить все меню ресторана по его ID
    public Collection<Menu> getMenuByRestId(int id) {
        session.beginTransaction();
        Collection<Menu> menuCollection = findRestById(id).getMenusById();
        return menuCollection;
    }

    //Удалить ресторан по его ID
    public void deleteRestById(int id) {
        Rests r = findRestById(id);
        Collection<Menu> m = r.getMenusById();
        session.beginTransaction();
        for (Menu l : m) {
            session.delete(l);
        }
        session.delete(r);
        session.getTransaction().commit();
    }

    //Получить все рестораны
    public List findAllRests() {
        Query restsList = session.createQuery("From Rests");
        return restsList.list();
    }

    public List findRestsByType(String type) {
        Query restsList = session.createQuery("From Rests t where t.type = \'" + type + "\'");
        return restsList.list();
    }

    public List findRestsByName(String name) {
        Query restsList = session.createQuery("From Rests t where t.name = \'" + name + "\'");
        return restsList.list();
    }

    public void addReviewToRest(Reviews r, int restid) {
        Rests rests = findRestById(restid);
        session.beginTransaction();
        Collection<Reviews> rv = rests.getReviewsById();
        rv.add(r);
        session.update(rests);
        session.getTransaction().commit();
    }

    public void addVisitToRest(Visits v, int restid) {
        Rests rests = findRestById(restid);
        session.beginTransaction();
        Collection<Visits> rv = rests.getVisitsById();
        rv.add(v);
        session.update(rests);
        session.getTransaction().commit();
    }

    public List<Reviews> findAllReviewsByRestId(int restid) {
        Rests r = findRestById(restid);
        return (List<Reviews>) r.getReviewsById();

    }

    public int findRestIdByWorker(int restworkerid) {
        Query q = session.createQuery("Select ar from AccsRests ar where ar.accountsId=:restworkerid");
        q.setParameter("restworkerid", restworkerid);
        AccsRests accsRests = (AccsRests) q.list().get(0);
        return accsRests.getRestsId();
    }
}

