package dao;

import entity.Menu;
import org.hibernate.Query;

import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class MenuDao extends DAO {

    //Найти Блюдо по его ID
    public Menu findMenuItemById(int id) {
        session.beginTransaction();
        Menu get = session.get(Menu.class, id);
        session.getTransaction().commit();
        return get;
    }

    //Сохранить новое блюдо
    public void saveNewMenuItem(Menu m) {
        session.beginTransaction();
        session.save(m);
        session.getTransaction().commit();
    }

    // Обновить информацию о блюде
    public void updateMenuItem(Menu m) {
        session.beginTransaction();
        session.update(m);
        session.getTransaction().commit();
    }

    //Удалить блюдо по его ID
    public void deleteMenuItemById(int id) {
        Menu m = findMenuItemById(id);
        session.beginTransaction();
        session.delete(m);
        session.getTransaction().commit();
    }

    //Получить все блюда
    public List findAllMenuItems(int restId) {
        Query Menu = session.createQuery("From Menu m where m.restsByRestsId.id = " + restId);
        return Menu.list();
    }
}

