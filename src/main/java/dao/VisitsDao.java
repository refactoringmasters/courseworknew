package dao;

import entity.*;
import org.hibernate.Query;

import javax.ejb.Stateless;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@Stateless
public class VisitsDao extends DAO {
    //Найти визит по ID
    public Visits findVisitById(int id) {
        session.beginTransaction();
        Visits get = session.get(Visits.class, id);
        session.getTransaction().commit();
        return get;
    }

    //Сохранить новый визит
    public void saveNewVisit(Visits visits) {
        session.beginTransaction();
        session.save(visits);
        session.getTransaction().commit();
    }

    // Обновить информацию о визите
    public void updateVisit(Visits visits) {
        session.beginTransaction();
        session.update(visits);
        session.getTransaction().commit();
    }

    //Удалить визит по его ID
    public void deleteVisitById(int id) {
        session.beginTransaction();
        Visits visits = findVisitById(id);
        session.delete(visits);
        session.getTransaction().commit();
    }

    //Получить все визиты
    public List<Visits> findAllVisits() {
        List<Visits> visits = (List<Visits>) session.createQuery("From Visits ").list();
        return visits;
    }

    public void setVisitForRestForAcc(Visits v, int accid, int restid) {
        AccsDao ac = new AccsDao();
        ac.addVisitToAcc(v, accid);
        Accs acc = ac.findAccById(accid);
        ac.destroySession();
        RestsDao rd = new RestsDao();
        rd.addVisitToRest(v, restid);
        Rests rest = rd.findRestById(restid);
        rd.destroySession();
        session.beginTransaction();
        v.setAccs(acc);
        v.setRestsByRestsId(rest);
        session.save(v);
        session.getTransaction().commit();
    }

    public void addNewOrderToVisitToRestToAcc(int visitid, Orderd orderd) {
        Visits v = findVisitById(visitid);
        Collection<Orderd> orderds = v.getOrderdCollection();
        orderds.add(orderd);
        v.setOrderdCollection(orderds);
        orderd.setVisit(v);
        session.beginTransaction();
        OrderDao od = new OrderDao();
        od.saveNewOrder(orderd);
    }

    public float getCheckForVisit(int visitid) {
        Visits v = findVisitById(visitid);
        Collection<Orderd> orderds = v.getOrderdCollection();
        Iterator it = orderds.iterator();
        float summ = 0;
        MenuDao md = new MenuDao();
        Query q = session.createSQLQuery("SELECT SUM(price) FROM menu m INNER JOIN (select * from orderd o where o.visit_id=:visitid ) ov ON ov.menuitemid = m.id");
        q.setParameter("visitid", visitid);
        List<Float> list = q.list();
        if (list.isEmpty()) {
            return -2;
        } else {
            return list.get(0);
        }
    }

    public Collection<Visits> findVisitsByAccsId(int accsId) {
        Query q = session.createQuery("Select e from Visits e where e.accs.id=:accsId");
        q.setParameter("accsId", accsId);
        return q.list();
    }

    public Visits findActiveVisitsByAccsId(int accsId) {
        Query q = session.createQuery("Select v from Visits v where v.accs.id=:accsId and v.isActive=true ");
        q.setParameter("accsId", accsId);
        List<Visits> x = q.list();
        return x.get(0);
    }

    public int getRestIdForActiveVisitByAccsId(int accid) {
        Visits v = findActiveVisitsByAccsId(accid);
        return v.getRestsByRestsId().getId();
    }

    public Collection<Visits> findActiveVisitsByRestsId(int restsId) {
        Query q = session.createQuery("Select v from Visits v where v.restsByRestsId.id=:restsId and v.isActive=true ");
        q.setParameter("restsId", restsId);
        return q.list();
    }
}


