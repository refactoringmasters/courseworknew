package dao;

import org.hibernate.Session;

import javax.persistence.EntityManager;

import static session.PersistenceSession.getEm;

public class DAO {
    EntityManager em=getEm();
    public Session session = (Session)em.getDelegate();
    public void destroySession(){
        this.session.close();
    }
}
