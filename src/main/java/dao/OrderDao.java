package dao;

import entity.*;
import org.hibernate.Query;
import org.hibernate.criterion.Order;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.awt.geom.QuadCurve2D;
import java.sql.SQLData;
import java.sql.Time;
import java.util.List;

@Stateless
public class OrderDao extends DAO {

    //Найти заказы по ID
    public Orderd findOrderById(int id) {
        Query q = session.createQuery("SELECT c FROM Orderd c WHERE c.id =:idi");
        q.setParameter("idi", id);
        return (Orderd) q.list().get(0);
    }

    //Сохранить новый заказ
    public void saveNewOrder(Orderd orderd) {
        session.beginTransaction();
        session.save(orderd);
        session.getTransaction().commit();
    }

    // Обновить информацию о заказе
    public void updateOrder(Orderd orderd) {
        session.beginTransaction();
        session.update(orderd);
        session.getTransaction().commit();
    }

    //Удалить заказ по его ID
    public void deleteOrderById(int id) {
        session.beginTransaction();
        Orderd orderd = findOrderById(id);
        session.delete(orderd);
        session.getTransaction().commit();
    }

    //Получить все заказы
    public List<Orderd> findAllOrderd() {
        Query q = session.createQuery("From Orderd ");
        return q.list();
    }

    public List<Orderd> getOrdersForRest(int restid) {
        RestsDao rd = new RestsDao();
        Rests r = rd.findRestById(restid);
        Query tq = session.createQuery("SELECT c FROM Orderd c WHERE c.rests.id =:restid");
        tq.setParameter("restid", restid);
        return tq.list();
    }

    public List<Orderd> getOrdersAcceptedForRest(int restid) {
        String status = "Accepted";
        RestsDao rd = new RestsDao();
        Rests r = rd.findRestById(restid);
        Query tq = session.createQuery("SELECT c FROM Orderd c WHERE c.rests.id =:restid and c.status=:status");
        tq.setParameter("status", status);
        tq.setParameter("restid", restid);
        return tq.list();
    }

    public void saveOrderWithRestAcc(int restid, int accid, int menuitemid, int visitid) {
        Orderd orderd = new Orderd();
        orderd.setStatus("Accepted");
        Time t = new Time(System.currentTimeMillis());
        AccsDao accsDao = new AccsDao();
        RestsDao restsDao = new RestsDao();
        VisitsDao vd = new VisitsDao();
        orderd.setAccs(accsDao.findAccById(accid));
        orderd.setOrdertime(t);
        MenuDao md = new MenuDao();
        Menu m = md.findMenuItemById(menuitemid);
        orderd.setVisit(vd.findVisitById(visitid));
        orderd.setMenuByMenuId(m);
        orderd.setRests(restsDao.findRestById(restid));
        saveNewOrder(orderd);
    }

    public List<Orderd> getOrdersForRestForAcc(int restid, int accid) {
        Query tq = session.createQuery("SELECT c FROM Orderd c WHERE c.rests.id = :restid and c.accs.id=:accid");
        tq.setParameter("accid", accid);
        return tq.setParameter("restid", restid).list();
    }
}








