package buisnessLayer;

import dao.LogDao;
import entity.KitLog;
import entity.RestsLog;
import entity.UsrLog;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;


@Stateless
@Path("/general")
public class General {
    @Inject
    LogDao logDao;

    @GET
    @Path("/checkcode")
    public Response checkCode(
            @QueryParam("role") String role,
            @QueryParam("id") int id,
            @QueryParam("code") String code
    ) {
        switch (role) {
            case "user": {
                UsrLog usrLog = logDao.findUsrLogById(id);
                if (usrLog.getCode().equals(code)) {
                    usrLog.setConfirmation(true);
                    logDao.updateUsrLog(usrLog);
                    return Response.ok().build();
                }
            }
            case "kitchen": {
                KitLog kitLog = logDao.findKitLogById(id);
                if (kitLog.getCode().equals(code)) {
                    kitLog.setConfirmation(true);
                    logDao.updateKitLog(kitLog);
                    return Response.ok().build();
                }

            }
            case "rest": {
                RestsLog restsLog = logDao.findRestLogById(id);
                if (restsLog.getCode().equals(code)) {
                    restsLog.setConfirmation(true);
                    logDao.updateRestLog(restsLog);
                    return Response.ok().build();
                }
            }

        }
        return Response.ok().status(Response.Status.FORBIDDEN).build();
    }
}
