package buisnessLayer;

import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.ObjectMapper;
import dao.AccsDao;
import dao.LogDao;
import entity.Accs;
import exception.NoSuchRoleException;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

@Stateless
@Path("/authentication")
public class AuthenticationEndpoint {
    AccsDao accsDao = new AccsDao();
    LogDao logDao = new LogDao();

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response authenticateUser(@FormParam("username") String username,
                                     @FormParam("password") String password,
                                     @FormParam("role") String role
    ) {
        password = org.apache.commons.codec.digest.DigestUtils.sha256Hex(password);
        try {
            authenticate(username, password, role);
            int id = accsDao.findAccByMail(username).getId();
            if (role.equals("user")) {
                String token = generateToken();
                token = "user" + token;
                Accs a = accsDao.findAccById(logDao.findUsrLogByMail(username).getId());
                logDao.findUsrLogById(id).setToken(token);
                logDao.updateUsrLog(logDao.findUsrLogById(id));
                a.token = token;
                ObjectMapper om = new ObjectMapper();
                String json = om.writeValueAsString(a);
                return Response.ok("{\"" + role + "\":" + json + "}").build();
            }
            if (role.equals("admin")) {
                String token = generateToken();
                token = "admin" + token;
                logDao.findAdminLogById(id).setToken(token);
                logDao.updateAdminLog(logDao.findAdminLogById(id));
                return Response.ok(token).build();
            }
            if (role.equals("kitchen")) {
                String token = generateToken();
                token = "kitchen" + token;
                logDao.findKitLogById(id).setToken(token);
                logDao.updateKitLog(logDao.findKitLogById(id));
                return Response.ok("{\"" + "user" + "\":{\"id\":" + id + ", \"email\": \"" + username + "\", \"token\": \"" + token + "\"} }").build();

            }
            if (role.equals("rest")) {
                String token = generateToken();
                token = "rest" + token;
                logDao.findRestLogById(id).setToken(token);
                logDao.updateRestLog(logDao.findRestLogById(id));
                return Response.ok("{\"" + role + "\":{\"id\":" + id + ", \"email\": \"" + username + "\", \"token\": \"" + token + "\"} }").build();
            }
        } catch (Exception e) {

        }
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }

    private void authenticate(String username, String password, String role) throws Exception {

        AccsDao accsDao = new AccsDao();
        LogDao logDao = new LogDao();
        int id = accsDao.findAccByMail(username).getId();

        if (role.equals("user")) {
            if (password.equals(logDao.findUsrLogById(id).getHash())) {
            } else {
                throw new IllegalAccessException() {
                };
            }
        } else if (role.equals("admin")) {
            if (password.equals(logDao.findAdminLogById(id).getHash())) {
            } else {
                throw new IllegalAccessException() {
                };
            }
        } else if (role.equals("kitchen")) {
            if (password.equals(logDao.findKitLogById(id).getHash())) {
            } else {
                throw new IllegalAccessException() {
                };
            }
        } else if (role.equals("rest")) {
            if (password.equals(logDao.findRestLogById(id).getHash())) {

            } else {
                throw new IllegalAccessException() {
                };
            }
        } else
            throw new NoSuchRoleException(" No such role ");
    }

    private String generateToken() {
        Random random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }
}