package buisnessLayer;

import configs.AuthenficationConfig;
import dao.*;
import entity.*;

import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

@Stateful
@Path("/admin")
public class Admin {
    @Inject
    LogDao logDao;
    @Inject
    RestsDao restsDao;
    @Inject
    AccsDao accsDao;
    @Inject
    EmailClient emailClient;

    @POST
    @Path("/addrestworker")
    public Response addRestWorker(
            @FormParam("password") String password,
            @FormParam("name") String name,
            @FormParam("surname") String surname,
            @FormParam("email") String email,
            @FormParam("mobile") String mobile,
            @FormParam("role") String role,
            @FormParam("restid") int restid
    ) {
        boolean validate = logDao.validateEmail(email);
        if (validate) {
            return Response.ok("Acc with this email already exists").build();

        }

        String code = emailClient.generateString();
        if (role.equals("rest")) {
            logDao.registrationRest(email, password, restid);
            RestsLog restsLog = logDao.findRestsLogByMail(email);
            restsLog.setCode(code);
            restsLog.setConfirmation(false);
            logDao.updateRestLog(restsLog);
            logDao.updateRestLog(restsLog);
        } else if (role.equals("kitchen")) {
            logDao.registrationKit(email, password, restid);
            KitLog kitLog = logDao.findKitLogByMail(email);
            kitLog.setCode(code);
            kitLog.setConfirmation(false);
            logDao.updateKitLog(kitLog);
            logDao.updateKitLog(kitLog);
        }
        try {
            int accid = accsDao.findAccByMail(email).getId();
            emailClient.sendAsHtml(email,
                    "ConfirmRegistration",
                    " Спасибо за регистрацию, ваш ссылка для подтверждения: " +
                            " http://macbook-air-ila.local:8080/rap/rest/general/checkcode?role=" + role
                            + "&id=" + accid
                            + "&code=" + code);
        } catch (MessagingException e) {
            return Response.ok("No such mail").build();
        }
        Accs accs = accsDao.findAccByMail(email);
        accs.setName(name);
        accs.setSurname(surname);
        accs.setRole(role);
        accs.setMobile(mobile);
        accsDao.updateAcc(accsDao.findAccByMail(email));
        return Response.ok().status(200).build();
    }

    @DELETE
    @Path("/deleterest")
    public Response deleteRestWorker(
            @FormParam("restid") int restid
    ) {
        restsDao.deleteRestById(restid);
        return Response.ok().status(200).build();

    }

    @DELETE
    @Path("/deletekit")
    public Response deleteKitWorker(
            @FormParam("kitid") int kitid
    ) {
        logDao.deleteKitLogById(kitid);
        accsDao.deleteAccById(kitid);
        return Response.ok().status(200).build();
    }

    @POST
    @Path("/addrestaurant")
    public Response addRestaurant(
            @FormParam("name") String name,
            @FormParam("address") String address,
            @FormParam("rating") float rating,
            @FormParam("description") String description,
            @FormParam("mobile") String mobile,
            @FormParam("type") String type,
            @FormParam("website") String website,
            @FormParam("averagecheque") float averagecheque,
            @FormParam("email") String email
    ) {
        Rests rests = new Rests();
        rests.setAddress(address);
        rests.setAverageCheque(averagecheque);
        rests.setMobile(mobile);
        rests.setDescription(description);
        rests.setRating(rating);
        rests.setType(type);
        rests.setName(name);
        rests.setWebsite(website);
        rests.setEmail(email);
        restsDao.saveNewRest(rests);
        return Response.ok().build();
    }

    @DELETE
    @Path("/deleterestaurant")
    public Response deleteRestaurant(
            @FormParam("restaurantid") int restaurantid
    ) {

        restsDao.deleteRestById(restaurantid);
        return Response.ok().build();

    }


    @POST
    @Path("/changeRestaurantInfo")
    public Response changeRestaurantInfo(
            @FormParam("name") String name,
            @FormParam("address") String address,
            @FormParam("rating") float rating,
            @FormParam("description") String description,
            @FormParam("mobile") String mobile,
            @FormParam("type") String type,
            @FormParam("website") String website,
            @FormParam("averagecheque") float averagecheque,
            @FormParam("email") String email,
            @FormParam("restid") int restid,
            @Context HttpHeaders headers

    ) {
        Rests rests = restsDao.findRestById(restid);
        rests.setAddress(address);
        rests.setAverageCheque(averagecheque);
        rests.setMobile(mobile);
        rests.setDescription(description);
        rests.setRating(rating);
        rests.setType(type);
        rests.setName(name);
        rests.setWebsite(website);
        rests.setEmail(email);
        restsDao.updateRest(rests);
        return Response.ok().build();
    }

    private int getAdminIdFromToken(@Context HttpHeaders headers) {
        String authorizationHeader =
                headers.getHeaderString(HttpHeaders.AUTHORIZATION);
        String token = authorizationHeader
                .substring(AuthenficationConfig.AUTHENTICATION_SCHEME.length()).trim();
        String tokenvalidation = logDao.checkTokenFindEmail(token);
        int accid = Integer.parseInt(tokenvalidation.substring("admin".length()).trim());
        return accid;
    }
}