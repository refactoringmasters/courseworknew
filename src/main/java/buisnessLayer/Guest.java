package buisnessLayer;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dao.RestsDao;
import entity.Rests;

import javax.ejb.Stateful;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Iterator;
import java.util.List;

@Stateful
@Path("/guest")
public class Guest {

    @GET
    @Path("/getrestaurants")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRestaurants() {
        RestsDao restsDao = new RestsDao();
        List<Rests> allRests = restsDao.findAllRests();
        String json = "";
        ObjectMapper on = new ObjectMapper();
        try {
            json = on.writeValueAsString(allRests);
        } catch (JsonProcessingException e) {
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        return Response.ok("{\"articles\":" + json + "," + "\"articlesCount\"" + ":" + allRests.size() + "}").build();
    }

    @GET
    @Path("/getrestaurant/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRestaurant(
            @PathParam(value = "name") String name
    ) {
        String json = "";
        ObjectMapper on = new ObjectMapper();
        try {
            RestsDao rd = new RestsDao();
            Rests q = (Rests) rd.findRestsByName(name).get(0);
            json = on.writeValueAsString(q);
        } catch (JsonProcessingException e) {
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        return Response.ok("{\"article\":" + json + "}").build();
    }

    @OPTIONS
    @Path("/getrestaurants")
    public Response opt() {


        return Response.ok().build();

    }

}

