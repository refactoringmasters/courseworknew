package buisnessLayer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import configs.AuthenficationConfig;
import dao.*;
import entity.*;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.*;

@Stateless
@Path("/user")
public class User {
    @Inject
    EmailClient emailClient;
    @Inject
    LogDao logDao;
    @Inject
    VisitsDao visitsDao;
    @Inject
    OrderDao orderDao;
    @Inject
    RestsDao restsDao;
    @Inject
    ReviewsDao reviewsDao;
    @Inject
    AccsDao accsDao;
    @Inject
    AuthenticationEndpoint authenticationEndpoint;


    @POST
    @Path("/signUp")
    public Response signUp(
            @FormParam("password") String password,
            @FormParam("name") String name,
            @FormParam("surname") String surname,
            @FormParam("email") String email,
            @FormParam("mobile") String mobile
    ) {
        try {
            boolean validate = logDao.validateEmail(email);
            if (validate) {
                return Response.ok("Acc with this email already exists").build();
            }
            logDao.registrationUsr(email, password, name);
            String code = emailClient.generateString();
            UsrLog usrLog = logDao.findUsrLogByMail(email);
            usrLog.setCode(code);
            usrLog.setConfirmation(false);
            logDao.updateUsrLog(usrLog);
            int accid = usrLog.getId();
            emailClient.sendAsHtml(email, "ConfirmRegistration",
                    " Спасибо за регистрацию, ваш ссылка для подтверждения: " +
                            " http://macbook-air-ila.local:8080/restapac/rest/general/checkcode?role=user&id=" + accid +
                            "&code=" + code);
            Accs accs = accsDao.findAccByMail(email);
            accs.setName(name);
            accs.setSurname(surname);
            accs.setMobile(mobile);
            accs.setRole("user");
            accsDao.updateAcc(accsDao.findAccByMail(email));
            return authenticationEndpoint.authenticateUser(email, password, "user");
        } catch (MessagingException e) {
            return Response.ok("No such mail").build();
        }
    }

    @POST
    @Path("/registration")
    public Response registration(
            @FormParam("password") String password,
            @FormParam("name") String name,
            @FormParam("email") String email
    ) {
        boolean validate = logDao.validateEmail(email);
        if (validate) {
            return Response.ok("Acc with this email already exists").build();
        }
        logDao.registrationUsr(email, password, name);
        String code = emailClient.generateString();
        UsrLog usrLog = logDao.findUsrLogByMail(email);
        usrLog.setCode(code);
        logDao.updateUsrLog(usrLog);
        int accid = usrLog.getId();
        try {
            emailClient.sendAsHtml(email, "ConfirmRegistration",
                    " Спасибо за регистрацию, ваш ссылка для подтверждения:" +
                            "  http://macbook-pro-nikita-4.local:8080/rap/rest/general/checkcode?role=user&id=" + accid +
                            "&code=" + code);
        } catch (MessagingException e) {
            return Response.ok("No such mail").build();
        }
        return Response.ok().build();
    }

    @POST
    @Path("/review/{slug}")
    public Response addReview(
            @FormParam("review") String text,
            @PathParam("slug") String slug,
            @Context HttpHeaders headers
    ) {
        int accid = getUserIdFromToken(headers);
        Rests rests = (Rests) restsDao.findRestsByName(slug).get(0);
        int restid = rests.getId();
        Reviews reviews = new Reviews();
        reviews.setText(text);
        reviews.setAccsId(accid);
        reviews.setRestsId(restid);
        reviewsDao.saveNewReview(reviews);
        try {
            emailClient.sendAsHtml(rests.getEmail(), "New review", text);
        } catch (MessagingException e) {
            return Response.ok("No such mail").build();
        }
        return Response.ok().status(200).build();
    }

    @POST
    @Path("/checkin/{slug}")
    public Response checkin(
            @PathParam("slug") String slug,
            @Context HttpHeaders headers,
            @FormParam("comment") String comment
    ) {
        try {
            int accid = getUserIdFromToken(headers);
            Accs accs = accsDao.findAccById(accid);
            UsrLog usrLog = logDao.findUsrLogById(accid);
            if (!usrLog.isConfirmation()) {
                return Response.ok("You need to confirm your acc").build();
            }
            String name = accs.getName();
            String Surname = accs.getSurname();
            Rests rests = (Rests) restsDao.findRestsByName(slug).get(0);
            int restid = rests.getId();
            emailClient.sendAsHtml(rests.getEmail(), "New Guest",
                    " New Guest in your restaurant Name: " + name +
                            " Surname: " + Surname +
                            ". Comment: " + comment +
                            " To apply follow the link http://localhost:8080/restapac/rest/rest/applycheckin?restid=" + restid +
                            "&userid=" + accid);
            return Response.ok("hhjaf" + comment).status(200).build();
        } catch (MessagingException e) {
            return Response.ok("No such mail").build();
        }
    }

    @GET
    @Path("/getreviews/{slug}")
    public Response getReviews(@PathParam("slug") String slug) {
        Rests rests = (Rests) restsDao.findRestsByName(slug).get(0);
        int restid = rests.getId();
        List<Reviews> reviews = reviewsDao.getReviewsForResr(restid);
        ObjectMapper objectMapper = new ObjectMapper();
        String json;
        try {
            json = objectMapper.writeValueAsString(reviews);
            return Response.ok(json).build();
        } catch (JsonProcessingException e) {
            return Response.ok("json mistake").build();
        }
    }

    @POST
    @Path("/order")
    public Response addOrder(
            @FormParam("rest_id") int restid,
            @FormParam("menuitem") int menuitem,
            @FormParam("visitid") int visitid,
            @Context HttpHeaders headers
    ) {
        {
            int accid = getUserIdFromToken(headers);
            orderDao.saveOrderWithRestAcc(restid, accid, menuitem, visitid);
            return Response.ok().status(200).build();
        }
    }

    @GET
    @Path("/cheque")
    public Response getCheque(
            @Context HttpHeaders headers
    ) {
        VisitsDao visitsDao = new VisitsDao();
        int accid = getUserIdFromToken(headers);
        visitsDao.getRestIdForActiveVisitByAccsId(accid);
        Visits visit = visitsDao.findActiveVisitsByAccsId(accid);
        int visitid = visit.getId();
        double summ;
        summ = visitsDao.getCheckForVisit(visitid);
        return Response.ok("{\"summ\":\"" + summ + "\"}").build();
    }

    @GET
    @Path("/orders")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOrders(
            @Context HttpHeaders headers
    ) {
        int restid;
        int accid = getUserIdFromToken(headers);
        VisitsDao vd = new VisitsDao();
        restid = vd.getRestIdForActiveVisitByAccsId(accid);
        OrderDao orderDao = new OrderDao();
        List<Orderd> orderds = orderDao.getOrdersForRestForAcc(restid, accid);
        StringWriter writer = new StringWriter();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        Map<String, Object> properties = new HashMap<String, Object>(2);
        properties.put("eclipselink.media-type", "application/json");
        properties.put("eclipselink.json.include-root", false);
        try {
            JAXBContext jc = JAXBContext.newInstance(new Class[]{Rests.class}, properties);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(orderds, writer);
        } catch (Exception e) {
        }
        return Response.ok(writer.toString()).build();
    }

    @POST
    @Path("/updateinfo")
    public Response updateinfo(
            @FormParam("password") String password,
            @FormParam("name") String name,
            @FormParam("surname") String surname,
            @FormParam("mobile") String mobile,
            @Context HttpHeaders headers) {
        int accid = getUserIdFromToken(headers);
        Accs accs = accsDao.findAccById(accid);
        accs.setName(name);
        accs.setSurname(surname);
        accs.setMobile(mobile);
        UsrLog usrLog = logDao.findUsrLogById(accid);
        String sha256hex = org.apache.commons.codec.digest.DigestUtils.sha256Hex(password);
        usrLog.setHash(sha256hex);
        logDao.updateUsrLog(usrLog);
        accsDao.updateAcc(accs);
        return Response.ok().build();
    }

    @POST
    @Path("/logout")
    public Response logout(@Context HttpHeaders headers) {
        int accid = getUserIdFromToken(headers);
        UsrLog usrLog = logDao.findUsrLogById(accid);
        usrLog.setToken(null);
        logDao.updateUsrLog(usrLog);
        return Response.ok().status(Response.Status.NO_CONTENT).build();
    }

    @GET
    @Path("/getrestaurants")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRestaurants() {
        List<Rests> allRests = restsDao.findAllRests();
        StringWriter writer = new StringWriter();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        Map<String, Object> properties = new HashMap<String, Object>(2);
        properties.put("eclipselink.media-type", "application/json");
        properties.put("eclipselink.json.include-root", false);
        try {
            JAXBContext jc = JAXBContext.newInstance(new Class[]{Rests.class}, properties);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(allRests, writer);
        } catch (Exception e) {
        }
        return Response.ok(writer.toString()).build();
    }

    @GET
    @Path("/getallvisits")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllVisits(@Context HttpHeaders headers) {
        int accid = getUserIdFromToken(headers);
        VisitsDao visitsDao = new VisitsDao();
        Collection<Visits> visits = visitsDao.findVisitsByAccsId(accid);
        String json;
        ObjectMapper on = new ObjectMapper();
        try {
            json = on.writeValueAsString(visits);
            return Response.ok(json).build();
        } catch (JsonProcessingException e) {
            return Response.ok("Troubles with json").build();
        }
    }

    @GET
    @Path("/getactivevisit")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getActiveVisit(@Context HttpHeaders headers) {
        int accid = getUserIdFromToken(headers);
        VisitsDao visitsDao = new VisitsDao();
        Visits visits = visitsDao.findActiveVisitsByAccsId(accid);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String json = objectMapper.writeValueAsString(visits);
            return Response.ok(json).build();
        } catch (JsonProcessingException e) {
            return Response.ok("Troubles with json").build();
        }
    }

    @GET
    @Path("/getusercheque")
    public Response getUserCheque(
            @Context HttpHeaders headers
    ) {
        int userid = getUserIdFromToken(headers);
        Visits visit = visitsDao.findActiveVisitsByAccsId(userid);
        int visitid = visit.getId();
        float summ;
        summ = visitsDao.getCheckForVisit(visitid);
        return Response.ok("{\"summ\":\"" + summ + "\"}").build();
    }

    @POST
    @Path("/endvisit")
    public Response endVisit(
            @Context HttpHeaders headers
    ) {
        int accid = getUserIdFromToken(headers);
        Visits visits = visitsDao.findActiveVisitsByAccsId(accid);
        visits.setIsActive(false);
        visitsDao.updateVisit(visits);
        return Response.ok().build();
    }

    private int getUserIdFromToken(@Context HttpHeaders headers) {
        String authorizationHeader =
                headers.getHeaderString(HttpHeaders.AUTHORIZATION);
        String token = authorizationHeader
                .substring(AuthenficationConfig.AUTHENTICATION_SCHEME.length()).trim();
        String tokenvalidation = logDao.checkTokenFindEmail(token);
        if (!tokenvalidation.equals("NOACC")) {
            int accid = Integer.parseInt(tokenvalidation.substring("user".length()).trim());
            return accid;
        } else return -1;
    }
}