package buisnessLayer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import configs.AuthenficationConfig;
import dao.*;
import entity.*;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;

@Stateless
@Path("/kitchen")
public class Kitchen {
    @Inject
    LogDao logDao;
    @Inject
    MenuDao menuDao;
    @Inject
    OrderDao orderDao;
    @Inject
    RestsDao restsDao;
    @Inject
    AccsDao accsDao;

    @POST
    @Path("/addmenu")
    public Response addMenu(
            @Context HttpHeaders headers,
            @FormParam("name") String name,
            @FormParam("composition") String composition,
            @FormParam("price") float price,
            @FormParam("availability") boolean availability
    ) {
        int accid = getKitchenIdFromToken(headers);
        int restarauntid = restsDao.findRestIdByWorker(accid);
        Menu menu = new Menu();
        menu.setName(name);
        menu.setAvailability(availability);
        menu.setComposition(composition);
        menu.setPrice(price);
        menu.setRestsByRestsId(restsDao.findRestById(restarauntid));
        menuDao.saveNewMenuItem(menu);
        return Response.ok().status(200).build();
    }

    @POST
    @Path("/removemenu")
    public Response removeMenu(
            @FormParam("menuitem") int menuitem
    ) {
        menuDao.deleteMenuItemById(menuitem);
        return Response.ok().status(200).build();
    }

    @POST
    @Path("/updatemenu")
    public Response updateMenu(@FormParam("itemid") int itemid,
                               @FormParam("name") String name,
                               @FormParam("composition") String composition,
                               @FormParam("price") float price,
                               @FormParam("availability") boolean availability) {
        Menu menu = menuDao.findMenuItemById(itemid);
        menu.setPrice(price);
        menu.setComposition(composition);
        menu.setName(name);
        menu.setAvailability(availability);
        menuDao.updateMenuItem(menu);
        return Response.ok().status(200).build();
    }

    @GET
    @Path("/ordersqueue")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOrderQueue(
            @Context HttpHeaders headers
    ) {
        int accid = getKitchenIdFromToken(headers);
        String json;
        int restid = restsDao.findRestIdByWorker(accid);
        List<Orderd> orderds = orderDao.getOrdersForRest(restid);
        ObjectMapper on = new ObjectMapper();
        try {
            json = on.writeValueAsString(orderds);
        } catch (JsonProcessingException e) {
            return Response.ok("Troubles with json").build();
        }
        return Response.ok(json).build();
    }

    @POST
    @Path("/changesordertatus")
    public Response changeStatus(
            @FormParam("status") String status,
            @FormParam("orderid") int orderid
    ) {
        Orderd orderd = orderDao.findOrderById(orderid);
        orderd.setStatus(status);
        orderDao.updateOrder(orderd);
        return Response.ok().build();
    }

    @DELETE
    @Path("/deleteOrder")
    public Response deleteOrder(
            @FormParam("orderid") int orderid
    ) {
        orderDao.deleteOrderById(orderid);
        return Response.ok().build();
    }

    @POST
    @Path("/logout")
    public Response logout(@Context HttpHeaders headers) {
        int accid = getKitchenIdFromToken(headers);
        KitLog kitLog = logDao.findKitLogById(accid);
        kitLog.setToken(null);
        logDao.updateKitLog(kitLog);
        return Response.ok().status(Response.Status.NO_CONTENT).build();
    }

    @GET
    @Path("/allmenu")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRestaurantMenu(@Context HttpHeaders headers) {
        int accid = getKitchenIdFromToken(headers);
        int restid = restsDao.findRestIdByWorker(accid);
        List<Menu> menu = menuDao.findAllMenuItems(restid);
        Collections.sort(menu, Menu.COMPAREBYID);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String json = objectMapper.writeValueAsString(menu);
            return Response.ok(json).build();
        } catch (JsonProcessingException e) {
            return Response.ok("Troubles with json").build();
        }
    }

    private int getKitchenIdFromToken(@Context HttpHeaders headers) {
        String authorizationHeader =
                headers.getHeaderString(HttpHeaders.AUTHORIZATION);
        String token = authorizationHeader
                .substring(AuthenficationConfig.AUTHENTICATION_SCHEME.length()).trim();
        String tokenvalidation = logDao.checkTokenFindEmail(token);
        int accid = Integer.parseInt(tokenvalidation.substring("kitchen".length()).trim());
        return accid;
    }
}
