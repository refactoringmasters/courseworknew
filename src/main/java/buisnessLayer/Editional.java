package buisnessLayer;

import configs.AuthenficationConfig;
import dao.AccsDao;
import dao.LogDao;
import entity.Accs;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("/editional")
public class Editional {
    @Inject
    LogDao logDao;
    @Inject
    AccsDao accsDao;

    @Path("/usera")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response userInfo(@Context HttpHeaders headers) {
        int id = getUserIdFromToken(headers);
        Accs accs = accsDao.findAccById(id);
        String username = accs.getEmail();
        String authorizationHeader =
                headers.getHeaderString(HttpHeaders.AUTHORIZATION);
        String token = authorizationHeader
                .substring(AuthenficationConfig.AUTHENTICATION_SCHEME.length()).trim();
        if (id == -1) {
            return Response.ok("NOACC").build();
        }
        return Response.ok("{\"" + "user" + "\":{\"id\":" + id + ", \"email\": \"" + username + "\", \"token\": \"" + token + "\"} }").build();
    }

    @Path("/kitchena")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response kitchenInfo(@Context HttpHeaders headers) {
        int id = getKitchenIdFromToken(headers);
        Accs accs = accsDao.findAccById(id);
        String username = accs.getEmail();
        String authorizationHeader =
                headers.getHeaderString(HttpHeaders.AUTHORIZATION);
        String token = authorizationHeader
                .substring(AuthenficationConfig.AUTHENTICATION_SCHEME.length()).trim();
        if (id == -1) {
            return Response.ok("NOACC").build();
        }
        return Response.ok("{\"" + "kitchen" + "\":{\"id\":" + id + ", \"email\": \"" + username + "\", \"token\": \"" + token + "\"} }").build();
    }

    @POST
    @Path("/addadmin")
    public Response addadmin(
            @FormParam("email") String email,
            @FormParam("password") String pass
    ) {
        LogDao logDao = new LogDao();
        logDao.registrationAdmin(email, pass);
        return Response.ok().build();
    }

    private int getUserIdFromToken(@Context HttpHeaders headers) {
        String authorizationHeader =
                headers.getHeaderString(HttpHeaders.AUTHORIZATION);
        String token = authorizationHeader
                .substring(AuthenficationConfig.AUTHENTICATION_SCHEME.length()).trim();
        String tokenvalidation = logDao.checkTokenFindEmail(token);


        if (!tokenvalidation.equals("NOACC")) {
            int accid = Integer.parseInt(tokenvalidation.substring("user".length()).trim());
            return accid;
        } else return -1;
    }

    private int getKitchenIdFromToken(@Context HttpHeaders headers) {
        String authorizationHeader =
                headers.getHeaderString(HttpHeaders.AUTHORIZATION);
        String token = authorizationHeader
                .substring(AuthenficationConfig.AUTHENTICATION_SCHEME.length()).trim();
        String tokenvalidation = logDao.checkTokenFindEmail(token);
        if (!tokenvalidation.equals("NOACC")) {
            int accid = Integer.parseInt(tokenvalidation.substring("kitchen".length()).trim());
            return accid;
        } else return -1;
    }


}
