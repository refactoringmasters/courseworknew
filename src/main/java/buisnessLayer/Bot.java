package buisnessLayer;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;
import dao.*;
import entity.*;

import java.util.ArrayList;

import java.util.List;

public class Bot extends TelegramLongPollingBot {

    /**
     * Метод для приема сообщений.
     *
     * @param update Содержит сообщение от пользователя.
     */
    @Override
    public void onUpdateReceived(Update update) {
        String message = update.getMessage().getText();
        sendMsg(update.getMessage().getChatId().toString(), message);
    }

    /**
     * Метод для настройки сообщения и его отправки.
     *
     * @param chatId id чата
     * @param s      Строка, которую необходимот отправить в качестве сообщения.
     */
    public synchronized void sendMsg(String chatId, String s) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);
        sendMessage.setText(s);
        RestsDao restsDao = new RestsDao();
        StringBuffer message_send = new StringBuffer();
        String menu = "Меню";
        String info = "Инфо";
        try {
            switch (s) {
                case "/start":
                    setButtons(sendMessage);
                    sendMessage.setText("Здравствуйте,мой господин, с помощью этого бота вы можете получить список всех заведений, список заведений по типу, а также посмотреть меню заведения "
                    );
                    sendMessage(sendMessage);
                    break;
                case "/help":
                    setButtons(sendMessage);
                    sendMessage.setText("Для вашего удобства, вы можете использловать кнопки. Для просмотра меню ресторана напишите \"Меню id\", где id - id интересуещего вас ресторана, который вам необходимо предварительно найти с помощью функции бота поиска по заведениям.");
                    sendMessage(sendMessage);
                    break;
                case "Все заведения":
                    setButtons(sendMessage);
                    List<Rests> allRests = restsDao.findAllRests();
                    for (int counter = 0; counter < allRests.size(); counter++) {
                        message_send.append(allRests.get(counter).getId())
                                .append("\t")
                                .append(allRests.get(counter).getName())
                                .append("\n");
                    }
                    sendMessage.setText(message_send.toString());
                    sendMessage(sendMessage);
                    break;
                case "Главное меню":
                    setButtons(sendMessage);
                    sendMessage.setText("Выберите");
                    sendMessage(sendMessage);

                    break;
                case "Выбрать заведения":
                    setButtons(sendMessage);
                    sendMessage.setText("Выберите");
                    sendMessage(sendMessage);
                    break;
                case "По типу":
                    setButtons(sendMessage);
                    sendMessage.setText("Выберите кнопками или напишите Бар/ресторан/кафе/столовая ");
                    sendMessage(sendMessage);
                    break;
                default:
                    if (s.startsWith(menu)) {
                        setButtons(sendMessage);
                        setButtons(sendMessage);
                        int restid = Integer.parseInt(sendMessage.getText().substring(menu.length()).trim());
                        List<Menu> m = (List<Menu>) restsDao.getMenuByRestId(restid);
                        for (int counter = 0; counter < m.size(); counter++) {
                            message_send.append(m.get(counter).getId())
                                    .append("\t")
                                    .append(m.get(counter).getName())
                                    .append("\n");
                        }
                        sendMessage.setText(message_send.toString());
                        sendMessage(sendMessage);
                        break;
                    } else if (s.toLowerCase().equals("бар") ||
                            s.toLowerCase().equals("ресторан") ||
                            s.toLowerCase().equals("столовая") ||
                            s.toLowerCase().equals("кафе")
                    ) {
                        setButtons(sendMessage);
                        String type = sendMessage.getText().toLowerCase();
                        List<Rests> restsByType = restsDao.findRestsByType(type);
                        for (int counter = 0; counter < restsByType.size(); counter++) {
                            message_send.append(restsByType.get(counter).getId())
                                    .append("\t")
                                    .append(restsByType.get(counter).getName())
                                    .append("\n");
                        }
                        sendMessage.setText(message_send.toString());
                        sendMessage(sendMessage);
                    } else if (s.startsWith(info)) {

                        setButtons(sendMessage);
                        setButtons(sendMessage);
                        int restid = Integer.parseInt(sendMessage.getText().substring(info.length()).trim());
                        Rests restsInfo = restsDao.findRestById(restid);
                        sendMessage.setText(restsInfo.getDescription());
                        sendMessage(sendMessage);
                        break;
                    } else {
                        setButtons(sendMessage);
                        sendMessage.setText("Я не понимаю вас, мой Господин, используйте /help, пожалуйста");
                        sendMessage(sendMessage);
                        break;
                    }
            }
        } catch (TelegramApiException e) {
        }
    }

    /**
     * Метод возвращает имя бота, указанное при регистрации.
     *
     * @return имя бота
     */
    @Override
    public String getBotUsername() {
        return "CsoonCorpBot";
    }

    /**
     * Метод возвращает token бота для связи с сервером Telegram
     *
     * @return token для бота
     */
    @Override
    public String getBotToken() {
        return "780932328:AAEnX2OW7QOTSZjmOb6C3lBqLYZGK2VSskQ";
    }

    public static void main(String[] args) {
        ApiContextInitializer.init();
        TelegramBotsApi tApi = new TelegramBotsApi();
        //ргеистрация бота
        try {
            tApi.registerBot(new Bot());
        } catch (TelegramApiRequestException g) {
            g.printStackTrace();
        }
    }

    public synchronized void setButtons(SendMessage sendMessage) {
        // Создаем клавиуатуру
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);
        // Создаем список строк клавиатуры
        List<KeyboardRow> keyboard = new ArrayList<>();
        // Первая строчка клавиатуры
        KeyboardRow keyboardFirstRow = new KeyboardRow();
        // Вторая строчка клавиатуры
        KeyboardRow keyboardSecondRow = new KeyboardRow();
        KeyboardRow keyboardThirdRow = new KeyboardRow();
        switch (sendMessage.getText()) {
            case "/start":
                keyboardFirstRow.removeAll(keyboard);
                keyboardSecondRow.removeAll(keyboard);
                keyboardThirdRow.removeAll(keyboard);
                // Добавляем кнопки в первую строчку клавиатуры
                keyboardFirstRow.add(new KeyboardButton("Выбрать заведения"));
                // Добавляем кнопки во вторую строчку клавиатуры
                keyboardSecondRow.add(new KeyboardButton("Посмотреть меню"));
                //keyboardThirdRow.add(new KeyboardButton("Главное меню"));
                // Добавляем все строчки клавиатуры в список
                keyboard.add(keyboardFirstRow);
                keyboard.add(keyboardSecondRow);
                //keyboard.add(keyboardThirdRow);
                // и устанваливаем этот список нашей клавиатуре
                replyKeyboardMarkup.setKeyboard(keyboard);
                break;

            case "Выбрать заведения":
                keyboardFirstRow.removeAll(keyboard);
                keyboardSecondRow.removeAll(keyboard);
                keyboardThirdRow.removeAll(keyboard);
                keyboardFirstRow.add("По типу");
                keyboardSecondRow.add("Все заведения");
                keyboardThirdRow.add("Главное меню");
                keyboard.add(keyboardFirstRow);
                keyboard.add(keyboardSecondRow);
                keyboard.add(keyboardThirdRow);
                replyKeyboardMarkup.setKeyboard(keyboard);
                break;

            case "Главное меню":
                keyboardFirstRow.removeAll(keyboard);
                keyboardSecondRow.removeAll(keyboard);
                keyboardThirdRow.removeAll(keyboard);
                // Добавляем кнопки в первую строчку клавиатуры
                keyboardFirstRow.add(new KeyboardButton("Выбрать заведения"));
                // Добавляем кнопки во вторую строчку клавиатуры
                keyboardSecondRow.add(new KeyboardButton("Посмотреть меню"));
                // Добавляем все строчки клавиатуры в список
                keyboard.add(keyboardFirstRow);
                keyboard.add(keyboardSecondRow);
                // и устанваливаем этот список нашей клавиатуре
                replyKeyboardMarkup.setKeyboard(keyboard);
                break;
            case "По типу":
                keyboardFirstRow.removeAll(keyboard);
                keyboardSecondRow.removeAll(keyboard);
                keyboardThirdRow.removeAll(keyboard);
                keyboardFirstRow.add("ресторан");
                keyboardFirstRow.add("кафе");
                keyboardSecondRow.add("бар");
                keyboardSecondRow.add("столовая");
                keyboardThirdRow.add(new KeyboardButton("Главное меню"));
                keyboard.add(keyboardFirstRow);
                keyboard.add(keyboardSecondRow);
                keyboard.add(keyboardThirdRow);
                replyKeyboardMarkup.setKeyboard(keyboard);
                break;
            default:
                keyboardFirstRow.removeAll(keyboard);
                keyboardSecondRow.removeAll(keyboard);
                keyboardThirdRow.removeAll(keyboard);
                keyboardThirdRow.add(new KeyboardButton("Главное меню"));
                keyboard.add(keyboardThirdRow);
                replyKeyboardMarkup.setKeyboard(keyboard);

                break;
        }
    }
}

