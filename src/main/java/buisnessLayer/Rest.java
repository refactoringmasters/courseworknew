package buisnessLayer;

import configs.AuthenficationConfig;
import dao.*;
import entity.*;

import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Stateful
@Path("/rest")
public class Rest {
    @Inject
    LogDao logDao;
    @Inject
    OrderDao orderDao;
    @Inject
    RestsDao restsDao;
    @Inject
    ReviewsDao reviewsDao;
    @Inject
    VisitsDao visitsDao;
    @Inject
    AccsDao accsDao;

    @GET
    @Path("/applycheckin")
    public Response applyCheckin(
            @QueryParam("restid") int restid,
            @QueryParam("userid") int userid
    ) {
        Visits visits = new Visits();
        visits.setRestsByRestsId(restsDao.findRestById(restid));
        visits.setIsActive(true);
        visits.setAccs(accsDao.findAccById(userid));
        visitsDao.saveNewVisit(visits);
        return Response.ok().status(200).build();
    }

    @POST
    @Path("/getusercheque")
    public Response getUserCheque(
            @FormParam("userid") int userid
    ) {
        visitsDao.getRestIdForActiveVisitByAccsId(userid);
        Visits visit = visitsDao.findActiveVisitsByAccsId(userid);
        int visitid = visit.getId();
        float summ;
        summ = visitsDao.getCheckForVisit(visitid);
        return Response.ok("{\"summ\":\"" + summ + "\"}").build();
    }

    @POST
    @Path("/addorder")
    public Response addOrder(
            @FormParam("userid") int userid,
            @FormParam("menuitem") int menuitem,
            @FormParam("visitid") int visitid,
            @Context HttpHeaders headers
    ) {
        {
            try {
                int accid = getRestIdFromToken(headers);
                int restarauntid = restsDao.findRestIdByWorker(accid);
                orderDao.saveOrderWithRestAcc(restarauntid, userid, menuitem, visitid);
                return Response.ok().status(200).build();
            } catch (Exception e) {
                e.printStackTrace();
                Response.ResponseBuilder response = Response.ok();
                response.status(401);
                return response.build();
            }
        }
    }

    @DELETE
    @Path("/deleteOrder")
    public Response deleteOrder(
            @FormParam("orderid") int orderid
    ) {
        orderDao.deleteOrderById(orderid);
        return Response.ok().build();
    }

    @GET
    @Path("/getreviews")
    public Response getReviews(
            @Context HttpHeaders headers
    ) {
        int accid = getRestIdFromToken(headers);
        int restid = restsDao.findRestIdByWorker(accid);
        List<Reviews> reviewsForRest = reviewsDao.getReviewsForResr(restid);
        StringWriter writer = new StringWriter();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        Map<String, Object> properties = new HashMap<String, Object>(2);
        properties.put("eclipselink.media-type", "application/json");
        properties.put("eclipselink.json.include-root", false);
        try {
            JAXBContext jc = JAXBContext.newInstance(new Class[]{Rests.class}, properties);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(reviewsForRest, writer);
        } catch (Exception e) {
        }
        return Response.ok(writer.toString()).build();
    }

    @POST
    @Path("/endvisit")
    public Response endVisit(
            @FormParam("visitid") int visit_id
    ) {
        Visits visits = visitsDao.findVisitById(visit_id);
        visits.setIsActive(false);
        visitsDao.updateVisit(visits);
        return Response.ok().build();
    }

    @POST
    @Path("/logout")
    public Response logout(@Context HttpHeaders headers) {
        int accid = getRestIdFromToken(headers);
        RestsLog restsLog = logDao.findRestLogById(accid);
        restsLog.setToken(null);
        logDao.updateRestLog(restsLog);
        return Response.ok().status(Response.Status.NO_CONTENT).build();
    }

    @GET
    @Path("/getactivevisits")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getActiveVisits(
            @Context HttpHeaders headers
    ) {
        int accid = getRestIdFromToken(headers);
        int restarauntid = restsDao.findRestIdByWorker(accid);
        visitsDao.findActiveVisitsByRestsId(restarauntid);
        //TODO JSON
        String json = "A";
        return Response.ok(json).build();
    }

    private int getRestIdFromToken(@Context HttpHeaders headers) {
        String authorizationHeader =
                headers.getHeaderString(HttpHeaders.AUTHORIZATION);
        String token = authorizationHeader
                .substring(AuthenficationConfig.AUTHENTICATION_SCHEME.length()).trim();
        String tokenvalidation = logDao.checkTokenFindEmail(token);
        int accid = Integer.parseInt(tokenvalidation.substring("rest".length()).trim());
        return accid;
    }
}
