package entity;

import javax.persistence.*;

@Entity
@Table(name = "AdminLog", schema = "public", catalog = "postgres")
public class AdminLog {
    private int id;
    private String hash;
    private Accs accsById;
    private String token;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "hash")
    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @Basic
    @Column(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdminLog adminLog = (AdminLog) o;

        if (id != adminLog.id) return false;
        if (hash != null ? !hash.equals(adminLog.hash) : adminLog.hash != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (hash != null ? hash.hashCode() : 0);
        return result;
    }

    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id", nullable = false)
    public Accs getAccsById() {
        return accsById;
    }

    public void setAccsById(Accs accsById) {
        this.accsById = accsById;
    }
}
