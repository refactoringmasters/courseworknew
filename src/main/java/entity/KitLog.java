package entity;

import javax.persistence.*;

@Entity
@Table(name = "kitlog", schema = "public", catalog = "postgres")
public class KitLog {
    private int id;
    private String hash;
    private Accs accsById;
    private String code;
    private String token;
    private boolean confirmation;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "hash")
    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @Basic
    @Column(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Basic
    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "confirmation")
    public boolean isConfirmation() {
        return confirmation;
    }

    public void setConfirmation(boolean confirmation) {
        this.confirmation = confirmation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KitLog kitLog = (KitLog) o;

        if (id != kitLog.id) return false;
        if (hash != null ? !hash.equals(kitLog.hash) : kitLog.hash != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (hash != null ? hash.hashCode() : 0);
        return result;
    }

    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id", nullable = false)
    public Accs getAccsById() {
        return accsById;
    }

    public void setAccsById(Accs accsById) {
        this.accsById = accsById;
    }
}
