package entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement
@Entity
public class Rests {

    public int id;

    private String name;

    private String photo;

    private String slug;

    private String address;

    private Float rating;

    private Float averageCheque;

    private String description;

    private String type;

    private String mobile;

    private String website;

    @JsonIgnore
    private Collection<AccsRests> accsRestsById;
    public Collection<Menu> menusById;
    @JsonIgnore

    private Collection<Reviews> reviewsById;
    @JsonIgnore
    private Collection<Visits> visitsById;

    private String email;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {

        this.slug = name;
        this.name = name;
    }

    @Basic
    @Column(name = "slug")
    public String getSlug() {
        return name;
    }

    public void setSlug(String name) {

        this.slug = name;
    }

    @Basic
    @Column(name = "photo")
    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Basic
    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "rating")
    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    @Basic
    @Column(name = "average_cheque")
    public Float getAverageCheque() {
        return averageCheque;
    }

    public void setAverageCheque(Float averageCheque) {
        this.averageCheque = averageCheque;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "mobile")
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Basic
    @Column(name = "website")
    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @OneToMany(mappedBy = "restsId")
    @JsonIgnore
    public Collection<AccsRests> getAccsRestsById() {
        return accsRestsById;
    }

    public void setAccsRestsById(Collection<AccsRests> accsRestsById) {
        this.accsRestsById = accsRestsById;
    }

    @OneToMany(mappedBy = "restsByRestsId", fetch = FetchType.EAGER)

    public Collection<Menu> getMenusById() {
        return menusById;
    }

    public void setMenusById(Collection<Menu> menusById) {
        this.menusById = menusById;
    }

    @OneToMany(mappedBy = "restsByRestsId")
    @JsonIgnore
    public Collection<Reviews> getReviewsById() {
        return reviewsById;
    }

    public void setReviewsById(Collection<Reviews> reviewsById) {
        this.reviewsById = reviewsById;
    }

    @OneToMany(mappedBy = "restsByRestsId")
    @JsonIgnore
    public Collection<Visits> getVisitsById() {
        return visitsById;
    }

    public void setVisitsById(Collection<Visits> visitsById) {
        this.visitsById = visitsById;
    }

    public void addMenuItem(Menu m) {


    }
}
