package entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Date;
import java.util.Collection;

@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement
@Entity
public class Visits {
    private int id;
    private Integer restsId;
    private Integer accsId;
    private Date dateVisit;

    private Rests restsByRestsId;
    @JsonIgnore
    private Accs accs;
    private Boolean isActive;

    private Collection<Orderd> orderdCollection;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "isActive")
    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean bol) {
        this.isActive = bol;
    }

    @Basic
    @Column(name = "date_visit")
    public Date getDateVisit() {
        return dateVisit;
    }

    public void setDateVisit(Date dateVisit) {
        this.dateVisit = dateVisit;
    }

    @ManyToOne
    @JoinColumn(name = "rests_id", referencedColumnName = "id")
    public Rests getRestsByRestsId() {
        return restsByRestsId;
    }

    public void setRestsByRestsId(Rests restsByRestsId) {
        this.restsByRestsId = restsByRestsId;
        this.restsId = restsByRestsId.getId();
    }

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "Accounts_id", referencedColumnName = "id")
    public Accs getAccs() {
        return accs;
    }

    public void setAccs(Accs accsByAccountsId) {
        this.accs = accsByAccountsId;
        this.accsId = accsByAccountsId.getId();
    }

    @OneToMany
    @JoinColumn(name = "visit_id")
    public Collection<Orderd> getOrderdCollection() {
        return orderdCollection;
    }

    public void setOrderdCollection(Collection<Orderd> od) {
        orderdCollection = od;
    }
}
