package entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Comparator;

@Entity
public class Menu {
    private int id;
    private String name;
    private String composition;
    private Float price;
    private Boolean availability;
    @JsonIgnore
    private Rests restsByRestsId;

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "id")

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "composition")
    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    @Basic
    @Column(name = "price")
    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    @Basic
    @Column(name = "avaliability")
    public Boolean getAvailability() {
        return availability;
    }

    public void setAvailability(Boolean availability) {
        this.availability =availability;
    }

    @ManyToOne(fetch =FetchType.EAGER)
    @JsonIgnore
    @JoinColumn(name = "rests_id", referencedColumnName = "id")
    public Rests getRestsByRestsId() {
        return restsByRestsId;
    }

    public void setRestsByRestsId(Rests restsByRestsId) {
        this.restsByRestsId = restsByRestsId;
    }

    public static final Comparator<Menu> COMPAREBYID = new Comparator<Menu>() {
        @Override
        public int compare(Menu lhs, Menu rhs) {
            return lhs.getId() - rhs.getId();
        }
    };
}
