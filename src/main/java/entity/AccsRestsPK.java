package entity;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class AccsRestsPK implements Serializable {
    private int restsId;
    private int accountsId;

    @Column(name = "rests_id")
    @Id
    public int getRestsId() {
        return restsId;
    }

    public void setRestsId(int restsId) {
        this.restsId = restsId;
    }

    @Column(name = "accounts_id")
    @Id
    public int getAccountsId() {
        return accountsId;
    }

    public void setAccountsId(int accountsId) {
        this.accountsId = accountsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccsRestsPK that = (AccsRestsPK) o;
        return restsId == that.restsId &&
                accountsId == that.accountsId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(restsId, accountsId);
    }
}
