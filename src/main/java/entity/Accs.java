package entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;

@Entity
public class Accs {

    public String token;
    private int id;
    private String name;
    private String surname;
    private Date dateBirth;
    private String email;
    private Date dateRegistration;
    private String photo;
    private String mobile;
    @JsonIgnore
    private Collection<AccsRests> accsRestsById;
    @JsonIgnore
    private AdminLog adminLogById;
    @JsonIgnore
    private KitLog kitLogById;
    @JsonIgnore
    private RestsLog restsLogById;
    @JsonIgnore
    private Collection<Reviews> reviewsById;
    @JsonIgnore
    private UsrLog usrLogById;

    @JsonIgnore
    private Collection<Visits> visitsById;
    private String role;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "surname")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Basic
    @Column(name = "date_birth")
    public Date getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(Date dateBirth) {
        this.dateBirth = dateBirth;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "date_registration")
    public Date getDateRegistration() {
        return dateRegistration;
    }

    public void setDateRegistration(Date dateRegistration) {
        this.dateRegistration = dateRegistration;
    }

    @Basic
    @Column(name = "photo")
    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Basic
    @Column(name = "mobile")
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Basic
    @Column(name = "role")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @JsonIgnore
    @OneToMany(mappedBy = "accountsId")
    public Collection<AccsRests> getAccsRestsById() {
        return accsRestsById;
    }

    public void setAccsRestsById(Collection<AccsRests> accsRestsById) {
        this.accsRestsById = accsRestsById;
    }

    @JsonIgnore
    @OneToOne(mappedBy = "accsById")
    public AdminLog getAdminLogById() {
        return adminLogById;
    }

    public void setAdminLogById(AdminLog adminLogById) {
        this.adminLogById = adminLogById;
    }

    @JsonIgnore
    @OneToOne(mappedBy = "accsById")
    public KitLog getKitLogById() {
        return kitLogById;
    }

    public void setKitLogById(KitLog kitLogById) {
        this.kitLogById = kitLogById;
    }

    @JsonIgnore
    @OneToOne(mappedBy = "accsById")
    public RestsLog getRestsLogById() {
        return restsLogById;
    }

    public void setRestsLogById(RestsLog restsLogById) {
        this.restsLogById = restsLogById;
    }

    @JsonIgnore
    @OneToMany(mappedBy = "accsByAccountsId")
    public Collection<Reviews> getReviewsById() {
        return reviewsById;
    }

    public void setReviewsById(Collection<Reviews> reviewsById) {
        this.reviewsById = reviewsById;
    }

    @JsonIgnore
    @OneToOne(mappedBy = "accsById")
    public UsrLog getUsrLogById() {
        return usrLogById;
    }

    public void setUsrLogById(UsrLog usrLogById) {
        this.usrLogById = usrLogById;
    }

    @JsonIgnore
    @OneToMany(mappedBy = "accs")
    public Collection<Visits> getVisitsById() {
        return this.visitsById;
    }

    public void setVisitsById(Collection<Visits> visitsById) {
        this.visitsById = visitsById;
    }
}
