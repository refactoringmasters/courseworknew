package entity;

import javax.persistence.*;

@Entity
@Table(name = "restslog", schema = "public", catalog = "postgres")
public class RestsLog {
    private int id;
    private String hash;
    private Accs accsById;
    private String token;
    private String code;
    private boolean confirmation;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "hash")
    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @Basic
    @Column(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Basic
    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "confirmation")
    public boolean isConfirmation() {
        return confirmation;
    }

    public void setConfirmation(boolean confirmation) {
        this.confirmation = confirmation;
    }

    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id", nullable = false)
    public Accs getAccsById() {
        return accsById;
    }

    public void setAccsById(Accs accsById) {
        this.accsById = accsById;
    }
}
