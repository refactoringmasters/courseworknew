package entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "accs_rests", schema = "public", catalog = "postgres")
@IdClass(AccsRestsPK.class)
public class AccsRests {
    @JsonBackReference
    private int restsId;
    private int accountsId;

    @Id
    @Column(name = "rests_id")
    @JsonBackReference
    public int getRestsId() {
        return restsId;
    }

    public void setRestsId(int restsId) {
        this.restsId = restsId;
    }

    @Id
    @Column(name = "accounts_id")
    public int getAccountsId() {
        return accountsId;
    }

    public void setAccountsId(int accountsId) {
        this.accountsId = accountsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccsRests accsRests = (AccsRests) o;
        return restsId == accsRests.restsId &&
                accountsId == accsRests.accountsId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(restsId, accountsId);
    }
}
