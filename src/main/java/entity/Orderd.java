package entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.internal.NotNull;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Time;

@Entity
@XmlRootElement(name = "AllOrder")
@XmlAccessorType(XmlAccessType.FIELD)

public class Orderd {
    @XmlElement(required = true)
    private int restid;
    @XmlElement(required = true)
    @JsonIgnore
    private Rests rests;
    @XmlElement(required = true)

    private Integer accid;
    @JsonIgnore
    @XmlElement(required = true)
    private Accs accs;
    @XmlElement(required = true)

    private Time ordertime;
    @XmlElement(required = true)
    private int id;
    @XmlElement(required = true)
    private String status;
    @XmlElement(required = true)
    @JsonIgnore
    private Visits visit;
    @XmlElement(required = true)
    private Menu menu;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "restid", referencedColumnName = "id")
    public Rests getRests() {
        return rests;
    }

    public void setRests(Rests rests) {
        restid = rests.getId();
        this.rests = rests;
    }

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "accid", referencedColumnName = "id")
    public Accs getAccs() {
        return accs;
    }

    public void setAccs(Accs accs) {
        this.accs = accs;
    }

    @ManyToOne
    @JoinColumn(name = "menuitemid", referencedColumnName = "id")
    public Menu getMenuByMenuId() {
        return menu;
    }

    public void setMenuByMenuId(Menu menu) {
        this.menu = menu;
    }

    @Basic
    @Column(name = "ordertime")
    public Time getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(Time ordertime) {
        this.ordertime = ordertime;
    }

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "visit_id", referencedColumnName = "id", nullable = false)
    public Visits getVisit() {
        return visit;
    }

    public void setVisit(Visits visit) {
        this.visit = visit;
    }


}
