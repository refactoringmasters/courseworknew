package entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name="AllReviews")
@XmlAccessorType(XmlAccessType.FIELD)
public class Reviews {
    @XmlElement(required = true)
    private int id;
    @XmlElement(required = true)
    private Integer restsId;
    @XmlElement(required = true)
    private Integer accsId;
    @XmlElement(required = true)
    private Float mark;
    @XmlElement(required = true)
    private String text;
    @XmlElement(required = true)
    @JsonIgnore
    private Rests restsByRestsId;
    @JsonIgnore
    private Accs accsByAccountsId;

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "rests_id")
    public Integer getRestsId() {
        return restsId;
    }

    public void setRestsId(Integer restsId) {
        this.restsId = restsId;
    }

    @Basic
    @Column(name = "accounts_id")
    public Integer getAccsId() {
        return accsId;
    }

    public void setAccsId(Integer accsId) {
        this.accsId = accsId;
    }

    @Basic
    @Column(name = "mark")
    public Float getMark() {
        return mark;
    }

    public void setMark(Float mark) {
        this.mark = mark;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "rests_id", referencedColumnName = "id",insertable = false ,updatable = false)
    public Rests getRestsByRestsId() {
        return restsByRestsId;
    }

    public void setRestsByRestsId(Rests restsByRestsId) {
        this.restsByRestsId = restsByRestsId;
        this.restsId=getRestsByRestsId().getId();
    }

    @ManyToOne(optional=false,cascade=CascadeType.ALL)
    @JoinColumn(name = "accounts_id", referencedColumnName = "id",insertable = false ,updatable = false)
    @JsonIgnore
    public Accs getAccsByAccountsId() {
        return accsByAccountsId;
    }

    public void setAccsByAccountsId(Accs accsByAccountsId) {
        this.accsByAccountsId = accsByAccountsId;
        this.accsId=accsByAccountsId.getId();
    }
}
